var socket = null;

function connect() {
    console.log("Begin connect");
    write("System", "Begin connect")    
    socket = new WebSocket("ws://localhost:50100/ws");

    socket.onerror = function() {
        write("System", "socket error")
        console.log("socket error");
    };

    socket.onopen = function() {
        write("System", "Connected");
    };

    socket.onclose = function(evt) {
        var explanation = "";
        if (evt.reason && evt.reason.length > 0) {
            explanation = "reason: " + evt.reason;
        } else {
            explanation = "without a reason specified";
        }

        write("System", "Disconnected with close code " + evt.code + " and " + explanation);
        setTimeout(connect, 5000);
    };

    socket.onmessage = function(event) {
        received(event.data.toString());
    };
}

function received(message) {
    write("Server", message);
}

function write(author, message) {
    var date = new Date()
    var dateStr = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds()

    var line = document.createElement("p");
    line.className = "message";
    line.textContent = dateStr + "  " + author + ": " + message;

    var messagesDiv = document.getElementById("messages");
    messagesDiv.appendChild(line);
    messagesDiv.scrollTop = line.offsetTop;
}

function onSend() {
    var input = document.getElementById("commandInput");
    if (input) {
        var text = input.value;
        if (text && socket) {
            socket.send(text);
            input.value = "";
            write("YOU", text)
        }
    }
}

function start() {
    connect();

    document.getElementById("sendButton").onclick = onSend;
    document.getElementById("commandInput").onkeydown = function(e) {
        if (e.keyCode == 13) {
            onSend();
        }
    };
}

function initLoop() {
    if (document.getElementById("sendButton")) {
        start();
    } else {
        setTimeout(initLoop, 300);
    }
}

initLoop();
